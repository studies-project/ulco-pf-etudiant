main :: IO ()
main = do
    putStr("What's your name : ")
    line <- getLine
    putStrLn("Hello " ++ line ++ "!")