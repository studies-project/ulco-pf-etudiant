import System.Environment

main :: IO ()
main = do
    args <- getArgs
    -- To have it in the same line we convert the [String] -> String
    -- putStr("Les arguments sonts : " ++ unwords args)
    -- Else in two lines
    putStr("Les arguments sonts : ")
    print args