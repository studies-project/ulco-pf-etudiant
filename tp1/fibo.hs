import System.Environment

main :: IO ()
main = do
    args <- getArgs
    if length args == 1
        then do 
            let number = head args
            putStr("f(" ++ number ++ ") = ")
            print (fibo (read number :: Int))
    else if length args < 1
        then putStrLn("usage: <n>")
    else
        putStrLn("Too much arguments")

fibo :: Int -> Int
fibo 0 = 0
fibo 1 = 1
fibo n = fibo(n - 1) + fibo(n - 2)