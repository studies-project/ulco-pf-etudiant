import System.Random

main :: IO ()
main = do
    -- Convert to a printable int (Integer -> Int)
    value <- randomRIO (0,100) :: IO Int
    print value