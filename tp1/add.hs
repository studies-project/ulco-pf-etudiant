main :: IO ()
main = do
    print (add2Int 2 3)
    print (add2Double 2 3)
    let u = 3 :: Int
    let v = 2 :: Double
    print (add2Num u u)
    print (add2Num v v)
    -- Because we use two different type of variable it won't work. 
    -- Num adapts to the type it was first given.
    print (add2Num u v)

-- I use the Integral type for the a variable and the function takes two a
-- then returns one a, each of these are Integrals. Here we don't want that.
-- a is useful if we wan't to have a more general type (Integral / Fractional )
-- add2Int ::  Integral a => a -> a -> a
add2Int :: Int -> Int -> Int
add2Int x y = x + y

add2Double :: Double -> Double -> Double
add2Double x y = x + y

-- We don't need to write because we don't use div or /, but it we precise it would be
add2Num :: Num a => a -> a -> a
add2Num x y = x + y