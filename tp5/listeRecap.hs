import Data.Char

main :: IO ()
main = do
    print $ multiples 35
    print $ combinaisons ["pomme", "poire"] [13, 37, 42]
    print $ tripletsPlyth 13

getHead1 :: [[a]] -> [a]
getHead1 xs = map head xd

getHead2 :: [[a]] -> [a]
getHead2 l = [x | (x:_) <- l]