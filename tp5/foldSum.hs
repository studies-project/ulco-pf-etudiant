import Data.Char

main :: IO ()
main = do
    print(foldSum2 [1..4])
    let str = "barfoo"
    -- $ or ()
    -- print $ foldl max (head str) (tail str)
    -- print $ foldl1 max str
    print (foldl max (head str) (tail str))
    print (foldl1 max str)

foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x:xs) = x + foldSum1 (xs)

foldSum2 :: [Int] -> Int
foldSum2 (xs) = foldr (+) 0 xs