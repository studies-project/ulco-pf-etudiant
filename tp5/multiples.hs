import Data.Char

main :: IO ()
main = do
    print $ multiples 35
    print $ combinaisons ["pomme", "poire"] [13, 37, 42]
    print $ tripletsPlyth 13

multiples :: Int -> [Int]
multiples x = [y | y <- [1..x], x `mod` y == 0]

combinaisons :: [a] -> [b] -> [(a, b)]
combinaisons a b = [(x, y) | x <- a, y <- b]

tripletsPlyth :: Int -> [(Int, Int, Int)]
tripletsPlyth a = [(x, y, z) | x <- [1..a], y <- [x..a], z <-[y..a], x*x+y*y == z*z]