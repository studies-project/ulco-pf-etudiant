import Data.Char

main :: IO ()
main = do
    print(mymap (toUpper) "toto")

mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x:xs) = 2*x : (mapDoubler1 xs)

mapDoubler2 :: [Int] -> [Int]
mapDoubler2 x = map (*2) x

mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs