import Data.Char

main :: IO ()
main = do
    print (filterEven2 [1..10])
    print (myfilter even [1..10])
    print (myfilter isLetter "toto5561")

filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x:xs) = if even x
                        then x : filterEven1 xs
                    else
                        filterEven1 xs


filterEven2 :: [Int] -> [Int]
filterEven2 x = filter even x

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x:xs) = if f x
                        then x : myfilter f xs
                    else
                        myfilter f xs