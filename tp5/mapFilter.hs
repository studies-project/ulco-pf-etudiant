import Data.Char

main :: IO ()
main = do
    print $ doubler [1..4]
    print $ pairs [1..4]

doubler :: [Int] -> [Int]
doubler xs = [x*2 | x <- xs]

pairs :: [Int] -> [Int]
pairs xs = [x | x <- xs, even x]