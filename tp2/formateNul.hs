main :: IO()
main = do
    putStrLn(formateNul2 0)
    putStrLn(formateNul2 1)

formateNul1 :: Int -> String
formateNul1 y = case x of
                    0 -> show x ++ " est nul"
                    _ -> show x ++ " est non nul"
                where x = y

formateNul2 :: Int -> String
formateNul2 0 = "0 est nul"
formateNul2 x = show x ++ " est non nul"
