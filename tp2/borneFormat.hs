main :: IO()
main = do
    putStrLn(borneEtFormate1 (0.5))
    putStr(borneEtFormate2 (0.5))

borneEtFormate1 :: Double -> String
borneEtFormate1 x =
    if x < 0
        then "0.0"
    else if x > 1
        then "1.1"
    else
        show x


borneEtFormate2 :: Double -> String
borneEtFormate2 x
    | x < 0 = "0.0"
    | x > 1 = "1.1"
    | otherwise = show x