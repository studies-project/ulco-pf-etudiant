import Text.Read

main :: IO()
main = do
    line <- getLine
    -- Precise Maybe Int to get only int
    case readMaybe line :: Maybe Int of
            Just x -> putStrLn("Just " ++ show x)
            Nothing -> putStrLn("Nothing")