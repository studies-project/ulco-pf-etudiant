import System.Environment

main :: IO()
main = do
    args <- getArgs
    if length args == 1
        then do 
            let result = read (head args) :: Int
            putStrLn(formaterParite result)
            putStr(formaterSigne result)
    else if length args < 1
        then putStrLn("usage: <n>")
    else
        putStrLn("Too much arguments")

formaterParite :: Int -> String
formaterParite x
    | (x `mod` 2 == 0) = "Pair"
    | otherwise = "Impair"

formaterSigne :: Int -> String
formaterSigne x
    | x < 0 = "Négatif"
    | x > 0 = "Positif"
    | otherwise = "nul"