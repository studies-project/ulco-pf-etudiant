import Text.Read

main :: IO()
main = do
    print (formatedSqrt (-16))
    print (formatedSqrt 16)

safeSqrt :: Double -> Maybe Double
safeSqrt x
    | x > 0 = Just (sqrt x)
    | otherwise = Nothing

formatedSqrt :: Double -> String
formatedSqrt x = "sqrt("++show x++")" ++ auxFormat (safeSqrt x)

auxFormat :: Maybe Double -> String
auxFormat Nothing = " is not defined"
auxFormat (Just x) = " = " ++ show x