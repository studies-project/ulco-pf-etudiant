import System.Environment

main :: IO()
main = do
    args <- getArgs
    if length args == 1
        then do 
            let result = read (head args) :: Int
            putStr(formaterSigne result)
    else if length args < 1
        then putStrLn("usage: <n>")
    else
        putStrLn("Too much arguments")

formaterParite :: Int -> String
formaterParite x = 
    if x `mod` 2 == 0
        then "pair"
    else
        "impair"

formaterSigne :: Int -> String
formaterSigne x = 
    if x < 0
        then "negatif"
    else if x > 0
        then "positif"
    else
        "nul"