myTake2 :: [a] -> [a]
myTake2 = take 2

main :: IO()
main = do
    print $ myTake2 [1..]
    print $ myTake2 "foobar"