main :: IO ()
main = do
    print $ myRangeTuple1 (1, 10)
    print $ myRangeCurry1 1 10
    print $ myRangeTuple2 10
    print $ myRangeCurry2 10


myRangeTuple1 :: (Int, Int) -> [Int]
myRangeTuple1 (x0, x1) = [x0 .. x1]

myRangeCurry1 :: Int -> Int -> [Int]
myRangeCurry1 x0 x1 = [x0 .. x1]

myRangeTuple2 :: Int -> [Int]
myRangeTuple2 x1 = myRangeTuple1 (0,x1)

myRangeCurry2 :: Int -> [Int]
-- myRangeCurry2 x1 = myRangeCurry1 0 x1
-- Mais pas besoin, on peut retirer x1 car la forme curryfiée le fait tout seul.
myRangeCurry2 = myRangeCurry1 0

