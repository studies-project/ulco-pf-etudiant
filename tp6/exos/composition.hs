plus42Positif x = x + 42 > 0
-- ou comme ça
plus42Positif = (>0) . (+42)