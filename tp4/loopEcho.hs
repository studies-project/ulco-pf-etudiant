main :: IO ()
main = do
    result <- (loopEcho 3)
    putStrLn(result)

loopEcho :: Int -> IO String
loopEcho 0 = return "loop terminated"
loopEcho n = do
    putStr "> "
    line <- getLine
    if (line == "")
        then return "empty line"
    else do
        putStrLn line
        loopEcho(n-1)