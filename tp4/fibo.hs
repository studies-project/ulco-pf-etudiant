main :: IO ()
main = do
    print (fiboNaive 35)

fiboNaive :: Int -> Int
fiboNaive 0 = 0
fiboNaive 1 = 1
fiboNaive n = fiboNaive(n - 1) + fiboNaive(n - 2)

-- To finish
fiboTco :: Int -> Int
fiboTco n = tempFibo n 0
    where   tempFibo 0 sum = sum
            tempFibo 1 sum = tempFibo 0 (sum + 1)
            tempFibo n sum = tempFibo(n - 1) (sum + tempFibo(n - 2))