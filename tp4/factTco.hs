main :: IO ()
main = do
    print (factTco 5)

{- Two ways work, or we let the local function outside factTco or we put it with a where.
factTco :: Int -> Int -> Int
factTco 1 res = res
factTco n res = factTco (n-1) (n * res)
-}

factTco :: Int -> Int
factTco x = tempFactTco x 1
    where   tempFactTco 1 res = res
    tempFactTco n res = tempFactTco (n-1) (n * res)