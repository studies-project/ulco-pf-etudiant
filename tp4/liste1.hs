import Data.Char

main :: IO ()
main = do
    print (onlyLetters "lorem ipsum")

myLength :: [a] -> Int
myLength [] = 0
myLength (_:xs) = 1 + myLength xs

toUpperString :: String -> String
toUpperString "" = ""
-- To concat the Char to the String : instead of ++
toUpperString (x:xs) = (toUpper x) : (toUpperString xs)

onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (x:xs) = if (isLetter x)
                        then x : (onlyLetters xs)
                    else
                        onlyLetters xs