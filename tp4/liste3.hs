import Data.Char

main :: IO ()
main = do
    print (sumListe [1.1, 2.5, 3.6, 4.8, 5.9] )

-- mulListe :: Int -> [Int] ->  [Int]
mulListe :: Num a => a -> [a] -> [a]
mulListe _ [] = []
mulListe y (x:xs) = y*x : (mulListe y xs)

-- selectListe :: (Int, Int) -> [Int] -> [Int]
-- Because we change the signature we need the Ord classe
selectListe :: (Num a, Ord a) => (a, a) -> [a] -> [a]
selectListe _ [] =  []
selectListe (a,b) (x:xs) =  if (x >= a && x <= b)
                                then x : selectListe (a,b) xs
                            else
                                selectListe (a,b) xs

-- Pour être générique, sinon avant c'était sumListe :: [Int] -> Int
sumListe :: Num a => [a] -> a
sumListe (x:xs) = auxSumListe (x:xs) 0
    where   auxSumListe [] res = res
            auxSumListe (t:ts) res = auxSumListe ts (res + t)