main :: IO ()
main = do
    print (pgcd 49 35)

pgcd :: Int -> Int -> Int
pgcd x y
    | (y == 0) = x
    | otherwise = pgcd y (x `mod` y)