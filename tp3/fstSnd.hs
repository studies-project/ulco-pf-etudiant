main :: IO ()
main = do
    print (myFst (1, 2))
    print (mySnd (2, "toto"))
    print (myFst3 ("test", 2, "toto"))

myFst :: (a,b) -> a
myFst (x, _) = x

mySnd :: (a,b) -> b
mySnd (_, x) = x

myFst3 :: (a,b,c) -> a
myFst3 (x, _, _) = x