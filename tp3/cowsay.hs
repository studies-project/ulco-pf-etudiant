import System.Environment

main :: IO()
main = do
    args <- getArgs
    if length args == 1
        then do 
            let result = read (head args) :: Int
            putStrLn(formaterParite result)
            putStr(formaterSigne result)
    else if length args < 1
        then putStrLn("usage: <n>")
    else
        putStrLn("Too much arguments")